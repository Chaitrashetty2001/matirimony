package com.training.matrimony.controller;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.training.matrimony.dto.RegisterDto;
import com.training.matrimony.exception.ProfileNotFoundException;
import com.training.matrimony.service.impl.ProfileRegisterServiceImpl;

import jakarta.validation.ValidationException;

@ExtendWith(SpringExtension.class)

class ProfileRegisterControllerTest {

	@Mock
	private ProfileRegisterServiceImpl profileRegisterService;

	@InjectMocks
	private ProfileRegisterController profileRegisterController;

	@Test
	void testNewProfileRegister_Negative() {

		RegisterDto registerDto = new RegisterDto("Invalid User");

		when(profileRegisterService.newProfile(registerDto)).thenThrow(ProfileNotFoundException.class);

		assertThrows(ProfileNotFoundException.class, () -> profileRegisterController.newProfileRegister(registerDto));
	}

	
}
