package com.training.matrimony.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.training.matrimony.dto.Gender;
import com.training.matrimony.dto.Religion;
import com.training.matrimony.dto.SearchDto;
import com.training.matrimony.entity.Profile;
import com.training.matrimony.service.ProfileSearchService;

@ExtendWith(SpringExtension.class)
public class ProfileSearchControllerTest {

	@Mock
	private ProfileSearchService profileSearchService;

	@InjectMocks
	private ProfileSearchController profileSearchController;

	@Test
	void testSearchProfiles_SuccessfulSearch() {
		SearchDto searchDto = createValidSearchDto();
		List<Profile> expectedProfiles = createSampleProfileList();

		when(profileSearchService.searchProfiles(searchDto)).thenReturn(expectedProfiles);

		ResponseEntity<List<Profile>> responseEntity = profileSearchController.searchProfiles(searchDto);

		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertEquals(expectedProfiles, responseEntity.getBody());

		verify(profileSearchService, times(1)).searchProfiles(searchDto);
	}

	@Test
	void testSearchProfiles_EmptyResult() {
		SearchDto searchDto = createValidSearchDto();

		when(profileSearchService.searchProfiles(searchDto)).thenReturn(Collections.emptyList());

		ResponseEntity<List<Profile>> responseEntity = profileSearchController.searchProfiles(searchDto);

		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertTrue(responseEntity.getBody().isEmpty());

		verify(profileSearchService, times(1)).searchProfiles(searchDto);
	}

	private SearchDto createValidSearchDto() {
		SearchDto searchDto = new SearchDto();
		searchDto.setGender(Gender.MALE);
		searchDto.setReligion(Religion.HINDU);

		return searchDto;
	}

	private List<Profile> createSampleProfileList() {
		Profile profile1 = new Profile();

		Profile profile2 = new Profile();

		return List.of(profile1, profile2);
	}
}
