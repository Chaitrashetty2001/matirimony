package com.training.matrimony.controller;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.training.matrimony.dto.InterestsProfileDto;
import com.training.matrimony.exception.ProfileNotFoundException;
import com.training.matrimony.service.impl.InterestProfileServiceImpl;

@ExtendWith(MockitoExtension.class)

class InterestProfileControllerTest {

	@Mock
	private InterestProfileServiceImpl impl;;

	@InjectMocks
	private InterestProfileController controller;

	@Test
	void testNewInterests() {
		InterestsProfileDto dto = new InterestsProfileDto();
		List<Long> interestedProfiles = new ArrayList<>();
		interestedProfiles.add(2L);
		interestedProfiles.add(3L);
		dto.setInterestedProfileId(interestedProfiles);
		dto.setProfileId(1L);

		when(impl.newInterestProfile(dto)).thenThrow(ProfileNotFoundException.class);
		assertThrows(ProfileNotFoundException.class, () -> controller.newInterests(dto));
	}
	
	
	@Test
	void testNewInterests1() {
		InterestsProfileDto dto = new InterestsProfileDto();
		List<Long> interestedProfiles = new ArrayList<>();
		interestedProfiles.add(2L);
		interestedProfiles.add(3L);
		dto.setInterestedProfileId(interestedProfiles);
		dto.setProfileId(1L);

		when(impl.newInterestProfile(dto)).thenThrow(ProfileNotFoundException.class);
		assertThrows(ProfileNotFoundException.class, () -> controller.newInterests(dto));
	}

}
