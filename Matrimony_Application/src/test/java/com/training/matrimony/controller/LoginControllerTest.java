package com.training.matrimony.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.training.matrimony.dto.LoginDto;
import com.training.matrimony.dto.ResponseDto;
import com.training.matrimony.service.ProfileLoginService;

@ExtendWith(MockitoExtension.class)
public class LoginControllerTest {
	
	@Mock
	private ProfileLoginService profileLoginService;
	@InjectMocks
	private  LoginController loginController;
	

	

//	    @Test
//	    public void testUserLogout_Success() {
//	        // Arrange
//	        ProfileLoginService profileLoginService = mock(ProfileLoginService.class);
//	        LoginController loginController = new LoginController();
//	        LoginDto logoutDto = new LoginDto(); // Provide a valid logoutDto for testing
//
//	        // Mock the behavior of profileLoginService.logout()
//	        when(profileLoginService.logout(logoutDto)).thenReturn(new ResponseDto("Logout successful",201));
//
//	        // Act
//	        ResponseEntity<ResponseDto> responseEntity = loginController.userLogout(logoutDto);
//
//	        // Assert
//	        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
//	        assertNotNull(responseEntity.getBody());
//	        assertEquals("Logout successful", responseEntity.getBody().getMessages());
//	    }
//	
}
