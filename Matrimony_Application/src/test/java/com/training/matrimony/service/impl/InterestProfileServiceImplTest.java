package com.training.matrimony.service.impl;

import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.training.matrimony.dto.InterestStatus;
import com.training.matrimony.dto.InterestsProfileDto;
import com.training.matrimony.entity.Profile;
import com.training.matrimony.entity.ProfileInterest;
import com.training.matrimony.exception.ProfileNotFoundException;
import com.training.matrimony.repository.ProfileRepository;
@ExtendWith(SpringExtension.class)
public class InterestProfileServiceImplTest {
	
	@Mock
	ProfileRepository profileRepository;
	
	
	@InjectMocks
	InterestProfileServiceImpl impl;
	
	
	@Test
	void testNewProfileInterestsProfileNotFound() {
		InterestsProfileDto dto = new InterestsProfileDto();
		List<Long> interestedProfiles = new ArrayList<>();
		interestedProfiles.add(2L);
		interestedProfiles.add(3L);
		dto.setInterestedProfileId(interestedProfiles);
		dto.setProfileId(1L);
 
		Profile profile1 = new Profile();
		profile1.setProfileId(1L);
 
		Profile profile2 = new Profile();
		profile2.setProfileId(2L);
 
		Profile profile3 = new Profile();
		profile3.setProfileId(3L);
 
		ProfileInterest interest = new ProfileInterest();
		interest.setProfile(profile3);
		interest.setInterestStatus(InterestStatus.PENDING);
		interest.setInterestedInProfile(profile2);
 
		ProfileInterest interest2 = new ProfileInterest();
		interest2.setProfile(profile1);
		interest2.setInterestedInProfile(profile2);
		interest2.setInterestStatus(InterestStatus.PENDING);
 
		List<ProfileInterest> profileInterest = new ArrayList<>();
		profileInterest.add(interest2);
		profileInterest.add(interest);
 
		Mockito.when(profileRepository.findByProfileId(1L)).thenThrow(ProfileNotFoundException.class);
 
		assertThrows(ProfileNotFoundException.class, () -> {
			impl.newInterestProfile(dto);
		});
 
	}
	
	
	
	

}
