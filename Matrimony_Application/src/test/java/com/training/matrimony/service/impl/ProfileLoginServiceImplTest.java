package com.training.matrimony.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.training.matrimony.dto.LoggedInStatus;
import com.training.matrimony.dto.LoginDto;
import com.training.matrimony.dto.ResponseDto;
import com.training.matrimony.entity.Profile;
import com.training.matrimony.exception.ProfileNotFoundException;
import com.training.matrimony.repository.ProfileRepository;

@ExtendWith(MockitoExtension.class)
class ProfileLoginServiceImplTest {

	@InjectMocks
	private ProfileLoginServiceImpl profileLoginService;

	@Mock
	private ProfileRepository profileRepository;

	@Test
	void testLoginFailure() {
		LoginDto loginDto = new LoginDto("abc@gmail.com", "hhhhh");
		Mockito.when(profileRepository.findByUserNameAndPassword(loginDto.getUserName(), loginDto.getPassword()))
				.thenReturn(null);

		assertThrows(ProfileNotFoundException.class, () -> profileLoginService.login(loginDto));
		verify(profileRepository, never()).save(any());
	}

	@Test
	void testLogoutUserNotFound() {
		LoginDto logoutDto = new LoginDto("abc", "password");
		Mockito.when(profileRepository.findByUserNameAndPassword(logoutDto.getUserName(), logoutDto.getPassword()))
				.thenReturn(null);

		assertThrows(NullPointerException.class, () -> profileLoginService.logout(logoutDto));
		verify(profileRepository, never()).save(any());
	}

	@Test
	void testLoginWithInvalidCredentials() {
		LoginDto loginDto = new LoginDto("", "$tF4567");
		Mockito.when(profileRepository.findByUserNameAndPassword(loginDto.getUserName(), loginDto.getPassword()))
				.thenReturn(null);

		assertThrows(ProfileNotFoundException.class, () -> profileLoginService.login(loginDto));
	}

	@Test
	void testLogoutWithValidCredentials() {
		LoginDto logoutDto = new LoginDto("validUsername", "validPassword");
		Profile profile = new Profile();
		profile.setLoggedInStatus(LoggedInStatus.LOGGEDIN);

		Mockito.when(profileRepository.findByUserNameAndPassword(logoutDto.getUserName(), logoutDto.getPassword()))
				.thenReturn(profile);

		ResponseDto response = profileLoginService.logout(logoutDto);

		assertEquals("LoggedOut successfully", response.getMessages());
		assertEquals(200, response.getCode());
		assertEquals(LoggedInStatus.LOGGEDOUT, profile.getLoggedInStatus());
	}

	@Test
	void testLogoutWithInvalidCredentials() {
		LoginDto logoutDto = new LoginDto("invalidUsername", "invalidPassword");
		Mockito.when(profileRepository.findByUserNameAndPassword(logoutDto.getUserName(), logoutDto.getPassword()))
				.thenReturn(null);

		assertThrows(NullPointerException.class, () -> profileLoginService.logout(logoutDto));
	}

}
