package com.training.matrimony.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.training.matrimony.dto.LoggedInStatus;
import com.training.matrimony.dto.ProfileDto;
import com.training.matrimony.dto.ResponseDto;
import com.training.matrimony.entity.Profile;
import com.training.matrimony.repository.DemographicInfoRepository;
import com.training.matrimony.repository.ProfileRepository;

@ExtendWith(SpringExtension.class)
class ProfileRegisterServiceImplTest {

	@InjectMocks
	private ProfileInformationServiceImpl profileInformationService;

	@Mock
	private ProfileRepository profileRepository;

	@Mock
	private DemographicInfoRepository demographicInfoRepository;

	@Test
	void testUpdateExistingProfileWithLoggedOutStatus() {
		ProfileDto profileDto = new ProfileDto();
		Long profileId = 2L;
		Profile existingProfile = new Profile();
		existingProfile.setLoggedInStatus(LoggedInStatus.LOGGEDOUT);
		existingProfile.setProfileId(profileId);
		Mockito.when(profileRepository.findByProfileId(profileId)).thenReturn(existingProfile);
		ResponseDto response = profileInformationService.updateExistingProfile(profileDto, profileId);
		assertEquals("Information Updated Successfully", response.getMessages());
		assertEquals(200, response.getCode());

	}

	@Test
	void testUpdateExistingProfileWithNullProfileDto() {
		Long profileId = 3L;
		assertThrows(NullPointerException.class,
				() -> profileInformationService.updateExistingProfile(null, profileId));
	}

	@Test
	void testUpdateExistingProfileWithInvalidProfileId() {
		ProfileDto profileDto = new ProfileDto();
		Long invalidProfileId = 5L;

		Mockito.when(profileRepository.findByProfileId(invalidProfileId)).thenReturn(null);

		assertThrows(NullPointerException.class,
				() -> profileInformationService.updateExistingProfile(profileDto, invalidProfileId));
	}
	
	@Test
	void testUpdateExistingProfileWithInvalidProfileId1() {
		ProfileDto profileDto = new ProfileDto();
		Long invalidProfileId = 5L;

		Mockito.when(profileRepository.findByProfileId(invalidProfileId)).thenReturn(null);

		assertThrows(NullPointerException.class,
				() -> profileInformationService.updateExistingProfile(profileDto, invalidProfileId));
	}
	
	
	
	

}
