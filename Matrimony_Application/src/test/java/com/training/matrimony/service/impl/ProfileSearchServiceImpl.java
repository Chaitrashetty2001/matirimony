//package com.training.matrimony.service.impl;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.springframework.test.context.junit.jupiter.SpringExtension;
//
//import com.training.matrimony.dto.SearchDto;
//import com.training.matrimony.entity.Profile;
//import com.training.matrimony.exception.NoMatchingProfileFoundException;
//import com.training.matrimony.repository.ProfileSearchRepository;
//@ExtendWith(SpringExtension.class)
//class ProfileSearchServiceImplTest {
//
//    @Mock
//    private ProfileSearchRepository profileRepository;
//    
//    @Mock
//    
//
//    @InjectMocks
//    private ProfileSearchServiceImpl profileSearchService;
//
//    
//    
//
//    @Test
//    void searchProfiles_PositiveScenario() {
//        SearchDto searchDto = new SearchDto("MALE", "Hindu", "Brahmin", "City", 50000, "Engineer");
//        List<Profile> mockProfiles = new ArrayList<>();
//        when(profileRepository.findByDemographicInfoGenderAndDemographicInfoReligionOrDemographicInfoCasteOrDemographicInfoCityOrSalaryOrProfession(
//                toString(), toString(), toString(), toString(), anyDouble(), toString()))
//                .thenReturn(mockProfiles);
//
//        
//        List<Profile> resultProfiles = profileSearchService.searchProfiles(searchDto);
//
//      
//        assertNotNull(resultProfiles);
//        assertEquals(mockProfiles, resultProfiles);
//        verify(profileRepository, times(1)).findByDemographicInfoGenderAndDemographicInfoReligionOrDemographicInfoCasteOrDemographicInfoCityOrSalaryOrProfession(
//                searchDto.getGender(), searchDto.getReligion(), searchDto.getCaste(), searchDto.getCity(),
//                searchDto.getSalary(), searchDto.getProfession());
//    }
//
//   
//
//	@Test
//    void searchProfiles_NegativeScenario() {
//     
//        SearchDto searchDto = new SearchDto("Male", "Hindu", "Brahmin", "City", 50000, "Engineer");
//        when(profileRepository.findByDemographicInfoGenderAndDemographicInfoReligionOrDemographicInfoCasteOrDemographicInfoCityOrSalaryOrProfession(
//                toString(), toString(), toString(), toString(),(), toString()))
//                .thenReturn(new ArrayList<>()); 
//
//     
//        assertThrows(NoMatchingProfileFoundException.class,
//                () -> profileSearchService.searchProfiles(searchDto));
//    }
//}
//
//
