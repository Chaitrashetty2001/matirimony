//package com.training.matrimony.service.impl;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.mockito.ArgumentMatchers.any;
//import static org.mockito.Mockito.verify;
//import static org.mockito.Mockito.when;
//
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.junit.jupiter.MockitoExtension;
//
//import com.training.matrimony.dto.LoggedInStatus;
//import com.training.matrimony.dto.ProfileDto;
//import com.training.matrimony.dto.ResponseDto;
//import com.training.matrimony.entity.DemographicInfo;
//import com.training.matrimony.entity.Profile;
//import com.training.matrimony.repository.DemographicInfoRepository;
//import com.training.matrimony.repository.ProfileRepository;
//
//@ExtendWith(MockitoExtension.class)
//class ProfileInformationServiceImplTest {
//
//	@Mock
//	private ProfileRepository profileRepository;
//
//	@Mock
//	private DemographicInfoRepository demographicInfoRepository;
//
//	@InjectMocks
//	private ProfileInformationServiceImpl profileInformationService;
//
//	@Test
//    void testUpdateExistingProfile() {
//        Profile existingProfile = new Profile();
//        existingProfile.setLoggedInStatus(LoggedInStatus.LOGGEDIN);
//        DemographicInfo existingDemographicInfo = new DemographicInfo();
//        existingProfile.setDemographicInfo(existingDemographicInfo);
//        when(profileRepository.findByProfileId(any(Long.class))).thenReturn(existingProfile);
//        ProfileDto profileDto = new ProfileDto();
//        profileDto.setAddress("123 Main St");
//        profileDto.setAge(25);
//        profileDto.setCaste("Some Caste");
//        Long profileId = 1L;
//        ResponseDto responseDto = profileInformationService.updateExistingProfile(profileDto, profileId);
//        assertEquals("Information Updated Successfully", responseDto.getMessages());
//        assertEquals(200, responseDto.getCode());
//        assertEquals(profileDto.getAddress(), existingDemographicInfo.getAddress());
//        assertEquals(profileDto.getAge(), existingDemographicInfo.getAge());
//        assertEquals(profileDto.getCaste(), existingDemographicInfo.getCaste());
//
//        verify(profileRepository).save(existingProfile);
//    }
//
//}
