package com.training.matrimony.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
 
@Getter
@Setter
@AllArgsConstructor
public class InformationDto {
 
	String userName;
	String email;
	String phoneNumber;
	Religion religion;
	
}
