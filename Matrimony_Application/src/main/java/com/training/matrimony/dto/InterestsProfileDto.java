package com.training.matrimony.dto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
 
@Getter
@Setter
public class InterestsProfileDto {
 
	Long profileId;
	List<Long> interestedProfileId;
 
}
 
