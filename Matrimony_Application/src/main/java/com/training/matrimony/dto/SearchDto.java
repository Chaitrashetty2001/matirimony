package com.training.matrimony.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SearchDto {

	private Gender gender;
	private Religion religion;
	private String caste;
	private String city;
	private Double salary;
	private String profession;

}
