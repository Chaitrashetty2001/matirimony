package com.training.matrimony.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RegisterDto {

	public RegisterDto(String email) {
		this.email=email;
	}
	
	public String email;
	
}
