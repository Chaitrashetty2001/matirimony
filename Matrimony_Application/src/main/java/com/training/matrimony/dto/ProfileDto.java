package com.training.matrimony.dto;

import java.time.LocalDate;

import jakarta.validation.constraints.Min;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProfileDto {

	String firstName;
	String lastName;
	String motherTongue;
	Religion religion;
	String caste;
	String subCaste;
	Gender gender;
	LocalDate dateOfBirth;
	Integer age;
	@Min(value = 10, message = " Phone number must be of 10 digit")
	String phoneNumber;
	String email;
	Double height;
	String address;
	String city;
	State state;
	MaritalStatus maritalStatus;
	Double salary;
	String profession;
}
