package com.training.matrimony.exception;

public class NoMatchingProfileFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;
private final int errCode;
	public NoMatchingProfileFoundException(String message, int errCode) {
		super(message);
		this.errCode = errCode;

	}

	public int getErrorCode() {
		return errCode;
	}

}
