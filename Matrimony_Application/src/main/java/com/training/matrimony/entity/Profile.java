package com.training.matrimony.entity;

import com.training.matrimony.dto.LoggedInStatus;
import com.training.matrimony.dto.MaritalStatus;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Profile {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long profileId;

	@OneToOne
	@JoinColumn(name = "demographic")
	DemographicInfo demographicInfo;

	private Double salary;

	private String profession;

	@Enumerated(EnumType.STRING)
	private MaritalStatus maritalStatus;

	private String userName;
	private String password;
	
	@Enumerated(EnumType.STRING)
	private LoggedInStatus loggedInStatus;

}
