package com.training.matrimony.entity;

import com.training.matrimony.dto.InterestStatus;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@Entity
public class ProfileInterest {
 
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long profileInterestId;
	@ManyToOne
	private Profile profile;
	@ManyToOne
	private Profile interestedInProfile;
	@Enumerated(EnumType.STRING)
	private InterestStatus interestStatus;

}
