package com.training.matrimony.entity;

import java.time.LocalDate;

import com.training.matrimony.dto.Gender;
import com.training.matrimony.dto.Religion;
import com.training.matrimony.dto.State;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class DemographicInfo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long demographicId;

	private String firstName;
	private String lastName;
	private String motherTongue;
	
	@Enumerated(EnumType.STRING)
	private Religion religion;
	
	private String caste;
	private String subCaste;
	
	@Enumerated(EnumType.STRING)
	private Gender gender;
	
	private LocalDate dateOfBirth;
	private Integer age;
	private String phoneNumber;
	private String email;
	private Double height;
	private String address;
	private String city;
	
	@Enumerated(EnumType.STRING)
	private State state;

}
