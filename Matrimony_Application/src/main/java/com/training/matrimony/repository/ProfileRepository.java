package com.training.matrimony.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.training.matrimony.dto.Gender;
import com.training.matrimony.dto.Religion;
import com.training.matrimony.entity.Profile;

public interface ProfileRepository extends JpaRepository<Profile, Long> {

	Profile findByProfileId(Long profileId);

	Profile findByUserNameAndPassword(String userName, String password);

	Object findByDemographicInfoGenderAndDemographicInfoReligionOrDemographicInfoCasteOrDemographicInfoCityOrSalaryOrProfession(
			Gender gender, Religion religion, String caste, String city, Double salary, String profession);

}
