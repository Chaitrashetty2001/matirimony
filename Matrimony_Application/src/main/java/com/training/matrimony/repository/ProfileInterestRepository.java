package com.training.matrimony.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.training.matrimony.entity.ProfileInterest;

public interface ProfileInterestRepository extends JpaRepository<ProfileInterest, Long> {

	ProfileInterest findByProfileProfileId(Long profileId);

	ProfileInterest findByInterestedInProfileProfileId(Long profile);

	ProfileInterest findByProfileProfileIdAndInterestedInProfileProfileId(Long profile, Long profileId);

	List<ProfileInterest> findAllByProfileProfileId(Long profileId);

	List<ProfileInterest> findAllByInterestedInProfileProfileId(Long profileId);

}
