package com.training.matrimony.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.training.matrimony.dto.Gender;
import com.training.matrimony.dto.Religion;
import com.training.matrimony.entity.Profile;

public interface ProfileSearchRepository extends JpaRepository<Profile, Long> {
	
	List<Profile> findByDemographicInfoGenderAndDemographicInfoReligionOrDemographicInfoCasteOrDemographicInfoCityOrSalaryOrProfession(
			Gender gender, Religion religion, String caste, String city, Double salary, String profession);

}
