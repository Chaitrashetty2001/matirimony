package com.training.matrimony.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.training.matrimony.entity.DemographicInfo;

public interface DemographicInfoRepository extends JpaRepository<DemographicInfo, Long>{

}
