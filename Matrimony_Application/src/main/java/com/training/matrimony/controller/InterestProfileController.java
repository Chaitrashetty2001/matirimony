package com.training.matrimony.controller;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
 
import com.training.matrimony.dto.InformationDto;
import com.training.matrimony.dto.InterestsProfileDto;
import com.training.matrimony.dto.ResponseDto;
import com.training.matrimony.service.impl.InterestProfileServiceImpl;

 
@RestController
@RequestMapping("/api")
public class InterestProfileController {
 
	InterestProfileServiceImpl interestProfileServiceImpl;
 
	public InterestProfileController(InterestProfileServiceImpl interestProfileServiceImpl) {
		super();
		this.interestProfileServiceImpl = interestProfileServiceImpl;
	}
 
	@PostMapping("/profile/interests")
	public ResponseDto newInterests(InterestsProfileDto dto) {
		return interestProfileServiceImpl.newInterestProfile(dto);
	}
 
	@PutMapping("/profile/interests")
	public ResponseDto updateInterests(InterestsProfileDto dto) {
		return interestProfileServiceImpl.updateInterestProfile(dto);
	}
 
	@GetMapping("/profile/interests")
	public List<InformationDto> getInterests(@RequestParam Long profileId) {
		return interestProfileServiceImpl.getInterests(profileId);
	}
	
	@GetMapping("/profile/requests")
	public List<InformationDto> getRequests(@RequestParam Long profileId) {
		return interestProfileServiceImpl.getRequestsFrom(profileId);
	}
}
