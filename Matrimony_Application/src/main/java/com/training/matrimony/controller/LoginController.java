package com.training.matrimony.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.training.matrimony.dto.LoginDto;
import com.training.matrimony.dto.ResponseDto;
import com.training.matrimony.service.ProfileLoginService;

import jakarta.validation.Valid;

@RestController
@RequestMapping("/api")
public class LoginController {
	@Autowired
	ProfileLoginService profileloginService;

	@PostMapping("/login")
	public ResponseEntity<ResponseDto> userLogin(@Valid @RequestBody LoginDto loginDto) {
		return new ResponseEntity<>(profileloginService.login(loginDto), HttpStatus.OK);
	}

	@PutMapping("/logout") 
	public ResponseEntity<ResponseDto> userLogout(@Valid @RequestBody LoginDto logoutDto) {
		return new ResponseEntity<>(profileloginService.logout(logoutDto), HttpStatus.OK);
	}

}