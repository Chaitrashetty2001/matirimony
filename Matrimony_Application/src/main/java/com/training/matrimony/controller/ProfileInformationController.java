package com.training.matrimony.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.training.matrimony.dto.ProfileDto;
import com.training.matrimony.dto.ResponseDto;
import com.training.matrimony.service.impl.ProfileInformationServiceImpl;


@RestController
@RequestMapping("/api")
public class ProfileInformationController {
@Autowired
	ProfileInformationServiceImpl profileInformationServiceImpl;

	public ProfileInformationController(ProfileInformationServiceImpl profileInformationServiceImpl) {
		super();
		this.profileInformationServiceImpl = profileInformationServiceImpl;
	}

	@PutMapping("/update/{profileId}") 
	public ResponseDto updateProfile(ProfileDto profileDto, @PathVariable Long profileId) {
		return profileInformationServiceImpl.updateExistingProfile(profileDto, profileId);
	}

}
