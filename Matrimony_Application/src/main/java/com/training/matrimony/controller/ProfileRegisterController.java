package com.training.matrimony.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.training.matrimony.dto.RegisterDto;
import com.training.matrimony.dto.RegisterResponseDto;
import com.training.matrimony.service.impl.ProfileRegisterServiceImpl;


@RestController
@RequestMapping("/api")
public class ProfileRegisterController {
@Autowired
	ProfileRegisterServiceImpl profileRegisterServiceImpl;

	public ProfileRegisterController(ProfileRegisterServiceImpl profileRegisterServiceImpl) {
		super();
		this.profileRegisterServiceImpl = profileRegisterServiceImpl;
	}

	@PostMapping("/profiles")
	public RegisterResponseDto newProfileRegister(RegisterDto registerDto) {
		return profileRegisterServiceImpl.newProfile(registerDto);
	}

}
