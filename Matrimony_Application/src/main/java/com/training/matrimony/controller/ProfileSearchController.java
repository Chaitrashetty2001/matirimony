package com.training.matrimony.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.training.matrimony.dto.SearchDto;
import com.training.matrimony.entity.Profile;
import com.training.matrimony.service.ProfileSearchService;

import jakarta.validation.Valid;

@RestController
@RequestMapping("/profiles")
@Validated
public class ProfileSearchController {

	    @Autowired
	    private ProfileSearchService profileSearchService;

	    @GetMapping("/search")
	    public ResponseEntity<List<Profile>> searchProfiles(@Valid  SearchDto searchDto) {
	        List<Profile> profiles = profileSearchService.searchProfiles(searchDto);
	        return new ResponseEntity<>(profiles, HttpStatus.OK);
	    }
	}


