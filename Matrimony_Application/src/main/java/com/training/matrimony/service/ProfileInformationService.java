package com.training.matrimony.service;

import com.training.matrimony.dto.ProfileDto;
import com.training.matrimony.dto.ResponseDto;

public interface ProfileInformationService {

	ResponseDto updateExistingProfile(ProfileDto profileDto, Long profileId);


}
