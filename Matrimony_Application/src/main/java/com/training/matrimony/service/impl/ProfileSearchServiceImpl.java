package com.training.matrimony.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.training.matrimony.dto.SearchDto;
import com.training.matrimony.entity.Profile;
import com.training.matrimony.exception.NoMatchingProfileFoundException;
import com.training.matrimony.repository.ProfileSearchRepository;
import com.training.matrimony.service.ProfileSearchService;

@Service
public class ProfileSearchServiceImpl implements ProfileSearchService {

	@Autowired
	private ProfileSearchRepository profileRepository;

	@Override
	public List<Profile> searchProfiles(SearchDto searchDto) {
		List<Profile> profiles = profileRepository
				.findByDemographicInfoGenderAndDemographicInfoReligionOrDemographicInfoCasteOrDemographicInfoCityOrSalaryOrProfession(
						searchDto.getGender(), searchDto.getReligion(), searchDto.getCaste(), searchDto.getCity(),
						searchDto.getSalary(), searchDto.getProfession());
		if (profiles.isEmpty()) {
			throw new NoMatchingProfileFoundException("No profiles found for the given search criteria.", 404);
		}
		return profiles;
	}

}
