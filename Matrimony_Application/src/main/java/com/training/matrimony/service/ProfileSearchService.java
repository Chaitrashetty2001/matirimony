package com.training.matrimony.service;

import java.util.List;

import com.training.matrimony.dto.SearchDto;
import com.training.matrimony.entity.Profile;

public interface ProfileSearchService {

	List<Profile> searchProfiles(SearchDto searchDto);

}
