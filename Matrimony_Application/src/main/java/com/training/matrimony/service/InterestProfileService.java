package com.training.matrimony.service;

import com.training.matrimony.dto.InterestsProfileDto;
import com.training.matrimony.dto.ResponseDto;

public interface InterestProfileService {
	
	ResponseDto newInterestProfile(InterestsProfileDto dto);
	 
	ResponseDto updateInterestProfile(InterestsProfileDto dto);

}
