package com.training.matrimony.service;

import com.training.matrimony.dto.LoginDto;
import com.training.matrimony.dto.ResponseDto;

public interface ProfileLoginService {
	
	ResponseDto login(LoginDto loginDto);

	ResponseDto logout(LoginDto logoutDto);

}
