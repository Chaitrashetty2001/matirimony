package com.training.matrimony.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.training.matrimony.dto.InformationDto;
import com.training.matrimony.dto.InterestStatus;
import com.training.matrimony.dto.InterestsProfileDto;
import com.training.matrimony.dto.ResponseDto;
import com.training.matrimony.entity.Profile;
import com.training.matrimony.entity.ProfileInterest;
import com.training.matrimony.exception.ProfileNotFoundException;
import com.training.matrimony.repository.ProfileInterestRepository;
import com.training.matrimony.repository.ProfileRepository;
import com.training.matrimony.service.InterestProfileService;

@Service
public class InterestProfileServiceImpl implements InterestProfileService {

	@Autowired
	ProfileRepository profileRepository;

	@Autowired
	ProfileInterestRepository interestRepository;

	@Override
	public ResponseDto newInterestProfile(InterestsProfileDto dto) {

		Profile loggedInProfile = profileRepository.findByProfileId(dto.getProfileId());

		if (Objects.isNull(loggedInProfile)) {
			throw new ProfileNotFoundException("Profile Not Found");
		}

		List<ProfileInterest> interestProfile = new ArrayList<>();
		for (Long profile : dto.getInterestedProfileId()) {
			Profile interestedInProfile = profileRepository.findByProfileId(profile);
			if (Objects.isNull(interestedInProfile))
				throw new ProfileNotFoundException("Profile with Id" + profile + "Not Found");

			ProfileInterest interest = new ProfileInterest();
			interest.setProfile(loggedInProfile);
			interest.setInterestStatus(InterestStatus.PENDING);
			interest.setInterestedInProfile(interestedInProfile);
			interestProfile.add(interest);
		}
		interestRepository.saveAll(interestProfile);
		return new ResponseDto("Profile Interests Request Successfull", 200);
	}

	@Override
	public ResponseDto updateInterestProfile(InterestsProfileDto dto) {

		ProfileInterest loggedInProfile = interestRepository.findByInterestedInProfileProfileId(dto.getProfileId());

		if (Objects.isNull(loggedInProfile)) {
			throw new ProfileNotFoundException("Profile Not Found");
		}

		List<ProfileInterest> interestProfile = new ArrayList<>();

		for (Long profile : dto.getInterestedProfileId()) {
			ProfileInterest interestedProfile = interestRepository
					.findByProfileProfileIdAndInterestedInProfileProfileId(profile, dto.getProfileId());
			if (Objects.isNull(interestedProfile))
				throw new ProfileNotFoundException("Profile with Id" + profile + "Not Found");

			interestedProfile.setInterestStatus(InterestStatus.ACCEPT);
			interestProfile.add(interestedProfile);
		}

		interestRepository.saveAll(interestProfile);
		return new ResponseDto("Profile Interests Update Request Successfull", 200);
	}

	public List<InformationDto> getInterests(Long profileId) {

		List<ProfileInterest> interest = interestRepository.findAllByProfileProfileId(profileId);
		List<Profile> interestedProfiles = interest.stream().map(t -> t.getInterestedInProfile()).toList();
		if (Objects.isNull(interest))
			throw new ProfileNotFoundException("Profile Not Found");
		List<InformationDto> information = new ArrayList<>();

		for (Profile profile : interestedProfiles) {
			information.add(new InformationDto(profile.getUserName(), profile.getDemographicInfo().getEmail(),
					profile.getDemographicInfo().getPhoneNumber(), profile.getDemographicInfo().getReligion()));
		}

		return information;
	}

	public List<InformationDto> getRequestsFrom(Long profileId) {

		List<ProfileInterest> interest = interestRepository.findAllByInterestedInProfileProfileId(profileId);
		List<Profile> requestProfiles = interest.stream().map(t -> t.getProfile()).toList();

		if (Objects.isNull(interest))
			throw new ProfileNotFoundException("Profile Not Found");

		List<InformationDto> information = new ArrayList<>();
		for (Profile profile : requestProfiles) {
			information.add(new InformationDto(profile.getUserName(), profile.getDemographicInfo().getEmail(),
					profile.getDemographicInfo().getPhoneNumber(), profile.getDemographicInfo().getReligion()));
		}
		return information;
	}

}
