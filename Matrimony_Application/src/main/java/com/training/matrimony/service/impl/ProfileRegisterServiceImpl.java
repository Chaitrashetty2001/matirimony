package com.training.matrimony.service.impl;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.training.matrimony.dto.LoggedInStatus;
import com.training.matrimony.dto.MaritalStatus;
import com.training.matrimony.dto.RegisterDto;
import com.training.matrimony.dto.RegisterResponseDto;
import com.training.matrimony.entity.DemographicInfo;
import com.training.matrimony.entity.Profile;
import com.training.matrimony.repository.DemographicInfoRepository;
import com.training.matrimony.repository.ProfileRepository;
import com.training.matrimony.service.ProfileRegisterService;

@Service
public class ProfileRegisterServiceImpl implements ProfileRegisterService {
@Autowired
	ProfileRepository profileRepository;
@Autowired
	DemographicInfoRepository demographicInfoRepository;

	public ProfileRegisterServiceImpl(ProfileRepository profileRepository,
			DemographicInfoRepository demographicInfoRepository) {
		super();
		this.profileRepository = profileRepository;
		this.demographicInfoRepository = demographicInfoRepository;
	}
 
	@Override 
	public RegisterResponseDto newProfile(RegisterDto registerDto) {

		Profile profile = new Profile();
		DemographicInfo demographicInfo = new DemographicInfo();
		profile.setDemographicInfo(demographicInfo);
		profile.setLoggedInStatus(LoggedInStatus.LOGGEDOUT);
		profile.setMaritalStatus(MaritalStatus.SINGLE);
		profile.setProfession(""); 
		profile.setSalary(0.0);
		profile.setUserName(registerDto.getEmail());
		String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789~`!@#$%^&*()-_=+[{]}\\|;:\'\",<.>/?";
		String pwd = RandomStringUtils.random(15, characters);
		profile.setPassword(pwd);
		profile.setProfession("");
		demographicInfoRepository.save(demographicInfo);
		profileRepository.save(profile);
		return new RegisterResponseDto(profile.getUserName(), pwd);
	}

}
