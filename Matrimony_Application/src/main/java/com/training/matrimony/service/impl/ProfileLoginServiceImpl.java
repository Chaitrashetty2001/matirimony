package com.training.matrimony.service.impl;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.training.matrimony.dto.LoggedInStatus;
import com.training.matrimony.dto.LoginDto;
import com.training.matrimony.dto.ResponseDto;
import com.training.matrimony.entity.Profile;
import com.training.matrimony.exception.ProfileNotFoundException;
import com.training.matrimony.repository.DemographicInfoRepository;
import com.training.matrimony.repository.ProfileRepository;
import com.training.matrimony.service.ProfileLoginService;
@Service
public class ProfileLoginServiceImpl implements ProfileLoginService {
	
	@Autowired
	ProfileRepository profileRepository;
	@Autowired
	DemographicInfoRepository demographicInfoRepository;
	
	
	public ResponseDto login(LoginDto loginDto) {
		org.slf4j.Logger logger = LoggerFactory.getLogger(getClass());
		System.out.println(loginDto.toString());
		Profile profile = profileRepository.findByUserNameAndPassword(loginDto.getUserName(), loginDto.getPassword());
		System.err.println(profile);
		if (profile != null && loginDto.getPassword().equals(profile.getPassword())) {
			profile.setLoggedInStatus(LoggedInStatus.LOGGEDIN);
			profileRepository.save(profile);
			logger.info("Profile Logged in successfully");
			return new ResponseDto("Logged In successfully", 200);
		} else {
			logger.error("Invalid Credentials or profile Not found");
			throw new ProfileNotFoundException("Invalid Credentials or profile Not Found.");
		}
 
	}

	public ResponseDto logout(LoginDto logoutDto) {

		Profile user = profileRepository.findByUserNameAndPassword(logoutDto.getUserName(), logoutDto.getPassword());
		if (user.getLoggedInStatus() == null)
			throw new ProfileNotFoundException("User Not Found please Register");
		else {
			user.setLoggedInStatus(LoggedInStatus.LOGGEDOUT);
			profileRepository.save(user);

			return new ResponseDto("LoggedOut successfully", 200);
		}

	}


}
