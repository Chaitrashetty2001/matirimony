package com.training.matrimony.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.training.matrimony.dto.LoggedInStatus;
import com.training.matrimony.dto.ProfileDto;
import com.training.matrimony.dto.ResponseDto;
import com.training.matrimony.entity.DemographicInfo;
import com.training.matrimony.entity.Profile;
import com.training.matrimony.repository.DemographicInfoRepository;
import com.training.matrimony.repository.ProfileRepository;
import com.training.matrimony.service.ProfileInformationService;

@Service
public class ProfileInformationServiceImpl implements ProfileInformationService{

	@Autowired
	ProfileRepository profileRepository;
	@Autowired
	DemographicInfoRepository demographicInfoRepository;
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	public ProfileInformationServiceImpl(ProfileRepository profileRepository) {
		super();
		this.profileRepository = profileRepository;
	}

	@Override
	public ResponseDto updateExistingProfile(ProfileDto profileDto, Long profileId) {
		
		
		Profile profile = profileRepository.findByProfileId(profileId);
		DemographicInfo demographicInfo= profile.getDemographicInfo();
		
		if(profile.getLoggedInStatus().equals(LoggedInStatus.LOGGEDIN)) {
		demographicInfo.setAddress(profileDto.getAddress());
		demographicInfo.setAge(profileDto.getAge());
		demographicInfo.setCaste(profileDto.getCaste());
		demographicInfo.setCity(profileDto.getCity());
		demographicInfo.setDateOfBirth(profileDto.getDateOfBirth());
		demographicInfo.setEmail(profileDto.getEmail());
		demographicInfo.setFirstName(profileDto.getFirstName());
		demographicInfo.setLastName(profileDto.getLastName());
		demographicInfo.setGender(profileDto.getGender());
		demographicInfo.setHeight(profileDto.getHeight());
		demographicInfo.setMotherTongue(profileDto.getMotherTongue());
		demographicInfo.setPhoneNumber(profileDto.getPhoneNumber());
		demographicInfo.setReligion(profileDto.getReligion());
		demographicInfo.setState(profileDto.getState());
		demographicInfo.setSubCaste(profileDto.getSubCaste());
		
		profile.setMaritalStatus(profileDto.getMaritalStatus());
		profile.setProfession(profileDto.getProfession());
		profile.setSalary(profileDto.getSalary());
		
		demographicInfoRepository.save(demographicInfo);
		profileRepository.save(profile);
		
	}
		logger.info("Profile Logged in successfully");
		return new ResponseDto("Information Updated Successfully", 200);
	}




	
}
