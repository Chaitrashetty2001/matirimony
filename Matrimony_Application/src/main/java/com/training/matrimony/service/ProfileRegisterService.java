package com.training.matrimony.service;

import com.training.matrimony.dto.RegisterDto;
import com.training.matrimony.dto.RegisterResponseDto;

public interface ProfileRegisterService {

	RegisterResponseDto newProfile(RegisterDto registerDto);

}
